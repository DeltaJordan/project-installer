﻿/*
 ProjectInstaller - https://gitlab.com/DeltaJordan/project-installer
 Copyright (C) 2022 DeltaJordan
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
using IWshRuntimeLibrary;
using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.IO;
using System.Reflection;
using System.Windows.Forms;
using File = System.IO.File;

namespace ProjectInstaller.Window
{
    public partial class Completed : Form
    {
        public Completed()
        {
            InitializeComponent();
        }

        public void Welcome_Closing(object sender, CancelEventArgs e) => QuitButton_Click(this, null);

        private void CreateShortcut(string path)
        {
            IWshShortcut shortcut = (IWshShortcut)new WshShell().CreateShortcut(path);

            shortcut.Description = "Project64 installed by ProjectInstaller";
            shortcut.TargetPath = Path.Combine(
                InstallerSettings.InstallDirectory,
                "PJ64Launcher.exe");

            shortcut.IconLocation = Path.Combine(
                InstallerSettings.InstallDirectory,
                "Project64.exe");

            shortcut.WorkingDirectory = InstallerSettings.InstallDirectory;

            shortcut.Save();
        }

        private void QuitButton_Click(object sender, EventArgs e)
        {
            this.Hide();

            File.WriteAllText(Path.Combine(Application.StartupPath, "installed.dat"), JsonConvert.SerializeObject(Assembly.GetExecutingAssembly().GetName().Version));

            if (TemporaryFilesCheckBox.Checked)
            {
                ComponentHelper.CleanupDownloadFiles(InstallerSettings.InstallerComponents);
            }

            Application.Exit();
        }
    }
}
